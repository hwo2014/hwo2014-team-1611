import json
import socket
import sys

from Circuit import *

class NoobBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        # for ai
        self.circuit = Circuit()

        self.turbo_available = False
        self.turbo_duration_ticks = 0
        self.turbo_duration_millisec = 0
        self.turbo_factor = 0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        print 'pingping'
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_my_car(self, data):
        self.ping()

    def on_game_init(self, data):
        self.circuit.initialize(data)
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    # this comes in every tick
    def on_car_positions(self, data):
        # update the car positions
        for car_position_info in data:
            id = car_position_info.get('id')
            car = self.circuit.cars[id.get('name')]
            car.update(car_position_info)

        # do the decision i.e. throttle, booster or so
        car = self.circuit.cars['HYUna']
        print car.piece_index, car.speed, car.angle
        piece = self.circuit.pieces[car.piece_index]

        angle = piece.get('angle')
        if angle:
            angle = abs(angle)
            print angle
            if angle > 50 :
                self.throttle(0.4)
            elif angle > 40 :
                self.throttle(0.58)
            elif angle > 30 :
                self.throttle(0.65)
            elif angle > 20 :
                self.throttle(0.68)
            elif angle > 10 :
                self.throttle(0.7)
            else :
                self.throttle(0.72)
        else:
            if abs(car.angle) > 3 :
                self.throttle(0.3)
            else :
                self.throttle(0.8)

    def on_crash(self, data):
        car = self.circuit.cars[data.get('name')]
        car.crashed()

        self.ping()

    def on_spawn(self, data):
        car = self.circuit.cars[data.get('name')]
        car.spawned()

        self.ping()

    def on_turbo_available(self, data):
        self.turbo_available = True
        self.turbo_duration_ticks = data.get('turboDurationTicks', 0)
        self.turbo_duration_millisec = data.get('turboDurationtMilliseconds', 0)
        self.turbo_factor = data.get('turboFactor', 0)

        self.ping()
        
    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_my_car,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type != 'carPositions':
                print msg_type, data
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
