import Circuit

class Car():
    def __init__(self, data):
        id = data.get('id')
        dimensions = data.get('dimensions')

        width = dimensions.get('width')
        height = dimensions.get('height')

        self.color = id.get('color')
        self.name = id.get('name')
        self.width = dimensions.get('width')
        self.height = dimensions.get('height')
        self.piece_index = 0
        self.distance = 0
        self.angle = 0
        self.start_lane_index = 0
        self.end_lane_index = 0
        self.lap = 0
        self.disabled = False
        self.speed = 0
        self.lane = 0

    def update(self, position_info):
        piece_position_info = position_info.get('piecePosition')
        lane = piece_position_info.get('lane')
        # calculate speed
        current_piece_index = piece_position_info.get('pieceIndex')
        if self.piece_index == current_piece_index :
            self.speed = piece_position_info.get('inPieceDistance') - self.distance
        else :
            c = Circuit.Circuit()
            piece = c.pieces[self.piece_index]
            self.speed = piece_position_info.get('inPieceDistance') + piece['length'] - self.distance 

        if self.speed < 0:
            print piece['length'], self.distance, position_info

        self.angle = position_info.get('angle')
        self.piece_index = piece_position_info.get('pieceIndex')
        self.start_lane_index = lane.get('startLaneIndex')
        self.end_lane_index = lane.get('endLaneIndex')

        self.distance = piece_position_info.get('inPieceDistance')

    def crashed(self):
        self.disabled = True
    def spawned(self):
        self.disabled = False
