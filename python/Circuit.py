from Car import *
from Singleton import *
from math import pi

@Singleton
class Circuit():
    def __init__(self):
        self.cars = dict()
        self.pieces = None
        self.switchable_pieces = None
        self.lanes = None

        self.num_of_lanes = 0
        self.name = None

    def initialize(self, circuit_info):
        race = circuit_info.get('race')
        if not race:
            raise Exception('Race info does not exists')
        
        track = race.get('track')
        if not track:
            raise Exception('Track info does not exists')

        pieces = track.get('pieces')
        if not pieces:
            raise Exception('Pieces info does not exists')

        lanes = track.get('lanes')
        if not lanes:
            raise Exception('Lane info does not exists')
        
        name = track.get('name')
        car_infos = race.get('cars')
        laps = race.get('raceSession').get('laps')

        for index in range(len(pieces)) :
            angle = pieces[index].get('angle')
            if angle :
                pieces[index]['length'] = abs(angle / 180 * pi * pieces[index]['radius'])

        self.pieces = pieces
        self.switchable_pieces = [p for p in pieces if p.get('switch', False)]
        self.lanes = lanes

        self.num_of_lanes = len(lanes)
        self.name = name
        for car_info in car_infos:
            self.add_car(car_info)

    def add_car(self, car_info):
        id = car_info.get('id')
        c = Car(car_info)
        self.cars[id.get('name')] = c
